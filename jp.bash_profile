
export NVM_DIR="/Users/jeffprinty/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
HISTFILESIZE=10000000
alias ..='cd ..'
alias ...='cd ../..'
alias g='git add . && git commit && echo "Push to repo? Enter for origin or enter remote name" && read remote && git push "$remote"'
alias gc='git clone'
alias l='ls -la'
alias rmf='rm -rf'
alias ns='npm start'
alias ni='npm install'
alias histg="history | grep"
alias flush="sudo killall -HUP mDNSResponder"
# Show directory name in iterm tab title
if [ $ITERM_SESSION_ID ]; then
  export PROMPT_COMMAND='echo -ne "\033];${PWD##*/}\007"; ':"$PROMPT_COMMAND";
fi
# Only load Liquid Prompt in interactive shells, not from a script or from scp
[[ $- = *i* ]] && source ~/liquidprompt/liquidprompt
if [ -f ~/.git-completion.bash ]; then
  . ~/.git-completion.bash
fi
alias busy='cat /dev/urandom | hexdump -C | grep "ca fe"'
