# Azul Codex #

## About
I wanted a place for us to share snippets and best practices.

I also wanted to play with shared team repos.

I can't figure out how that backtick got there and I can't seem to rename the project. whoops

## Links for Reference
* [azulsquad / azulcodex — Bitbucket](https://bitbucket.org/azulsquad/azulcodex)
* [Lost in the Ether · Navigating EPUB CFIs – Part 1](http://matt.garrish.ca/2013/03/navigating-cfis-part-1/)
* [Lost in the Ether · Navigating EPUB CFIs – Part 2](http://matt.garrish.ca/2013/12/navigating-cfis-part-2/)
* [A Complete Guide to Flexbox | CSS-Tricks](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
* [Delivering Octicons with SVG · GitHub](https://github.com/blog/2112-delivering-octicons-with-svg)
* [An odyssey to find a sustainable icon system with SVGs in React](http://blog.goguardian.com/nerds/an-odyssey-to-find-a-sustainable-icon-system-with-svgs-in-react)
* [Creating an SVG Icon System with React | CSS-Tricks](https://css-tricks.com/creating-svg-icon-system-react/)
* [Modularise CSS the React way – Medium](https://medium.com/@jviereck/modularise-css-the-react-way-1e817b317b04#.colbxqlsp)
* [Why You Shouldn't Style React Components With JavaScript](http://jamesknelson.com/why-you-shouldnt-style-with-javascript/)
* [Lifting State Up - React](https://facebook.github.io/react/docs/lifting-state-up.html)
* [Execution sequence of a React component's lifecycle methods | JavaScript](http://javascript.tutorialhorizon.com/2014/09/13/execution-sequence-of-a-react-components-lifecycle-methods/)
* [LearnKit Web API – VitalSource Developer Network](https://developer.vitalsource.com/hc/en-us/articles/206997448-LearnKit-Web-API#createMarker)
* [React.createClass versus extends React.Component](https://toddmotto.com/react-create-class-versus-component/)
